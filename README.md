GHID INSTALARE
========================
Download si drag-n-drop in browser: 
* Firefox: [matura-gunoi.xpi](https://github.com/cogloch/matura-gunoi/blob/master/matura-gunoi.xpi?raw=true)
* Chrome: [matura-gunoi.crx](https://github.com/cogloch/matura-gunoi/blob/master/matura-gunoi.crx?raw=true) - drag&drop pe pagina chrome://extensions/
* Opera: [matura-gunoi.nex](https://github.com/cogloch/matura-gunoi/blob/master/matura-gunoi.nex?raw=true)


Gala Cacat-o-metrului
========================
Premii
------------------------
* Cel mai de necacat site de stiri popular: hotnews/digi24 (nici nu i-am pus pe radar pentru ca cel mai probabil daca s-ar gasi ceva, ar fi fals-pozitiv, sau cum se zice pe romaneste; daca vede cineva o proportie cacat-o-stiri/stiri >= jumate din ce-i pe adevarul(care-s decenti) ii bag si pe astia)
* Cel mai de cacat site de stiri:   realitatea      (serios fratilor, ce plm)
* Cel mai hidos site:               gandul
* CEL MAI URLATOR SITE:             evz

Mentiuni speciale
------------------------
* Cea mai scarba sursa:   yahoo  
* Cea mai proasta sursa:  stirileprotv
* Cel mai turbo-cacat titlu: "  FOTO Știe cum să se promoveze: ”Cel mai scandalos lucru la mine e corpul meu” :D  "
* Cel mai de fain site care ar trebui sa fie mai popular: agrepres 


TODO 
========================
* regex
* Not-dumb pattern recognition: Cacat-o-metru(tm)
* Mobile 
* Browser icon thingy with setting for cacatometer threshold, option to (un)cacatify, number of cacatometer positives
* Uncacatify code itself 
* Sentiment analysis(?)
* Uncacatify only front pages(not articles too)


Likely false positives 
========================
* !               -> quotations 
* secret          -> "servicii secrete"
* Cum             -> "Cum e situatia in alte tari"

Cacat-o-metru
========================  
* Is it strong? Are you angry? Are you intensely hoping that the information turns out to be true? False?
* Does it use excessive punctuation(!!) or ALL CAPS for emphasis?
* Does it make a claim about containing a secret or telling you something that “the media” doesn’t want you to know?
* Is this information designed for easy sharing, like a meme?

(TODO: find source for this)

Alternativa
======================== 
https://www.facebook.com/groups/1591449631185716/permalink/1692075144456497/ 
https://chrome.google.com/webstore/detail/de-necrezut/djfkicajooephlfmebeckinidakdichn
