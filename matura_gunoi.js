var XhrRequest = function (url, type, callback) 
{
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        callback(this.responseText);
    };
    xhr.open(type, url);
    xhr.send();
};

var bCacatFree = false;

var UncacatifyByClass = function()
{
	bCacatFree = false;

	for(var i = 0; i < siteData.length; i++)
	{
		var matchingItems = document.getElementsByClassName(siteData[i].identifier);
			
		for(var itemIdx = 0; itemIdx < matchingItems.length; itemIdx++)
		{
			var headlines = matchingItems[itemIdx].getElementsByTagName("a");
			for(var headlineIdx = 0; headlineIdx < headlines.length; headlineIdx++)
			{
				if(CacatOMetru(headlines[headlineIdx].textContent) > cacatometruThreshold)
				{
					matchingItems[itemIdx].parentNode.removeChild(matchingItems[itemIdx]);
					break;
				}
			}
		}
	}

	bCacatFree = true;
};

var UncacatifyBySelector = function()
{
	bCacatFree = false;

	for(var i = 0; i < siteData.length; i++)
	{
		// Keep "identifier" on, will probably eventually figure out how to directly access subelement headlines using the json data
		var matchingItems = document.querySelectorAll(siteData[i].identifier); 
		
		if(siteData[i].cacatBaseline === "super cacat")
		{
			for(var idx = 0; idx < matchingItems.length; idx++)
			{
				matchingItems[idx].remove();
				break;
			}
			continue;
		}

		for(var itemIdx = 0; itemIdx < matchingItems.length; itemIdx++)
		{
			//var headline = matchingItems[itemIdx].getElementsByClassName(newsItemSelectorsArr[i].headline)[0];
			//console.log(headline);
			//console.log(headline.textContent); // TODO: de ce cacatu masii nu vrea aici, da in consola da  

			var headlines = matchingItems[itemIdx].getElementsByTagName("a");
			for(var headlineIdx = 0; headlineIdx < headlines.length; headlineIdx++)
			{
				if(CacatOMetru(headlines[headlineIdx].textContent) > cacatometruThreshold)
				{
					matchingItems[itemIdx].remove();
					break;
				}
			}
		}
	}

	bCacatFree = true;
};

var siteData;
var cacatData;

var funcUncacatify;

var bPermissive = false;

var observer = new MutationObserver(function(mutations) {
	if(bCacatFree) // TODO: look into how async in js works 
		funcUncacatify();
});

// ENTRY POINT 
// NOTE: LIMBAJ DE CACAT
XhrRequest(chrome.extension.getURL("cacat.json"), 'GET', function(cacatDataText) {
	cacatData = JSON.parse(cacatDataText);

	XhrRequest(chrome.extension.getURL("sites.json"), 'GET', function(siteDataText) { 
		var sitesDataArr = JSON.parse(siteDataText).sites; 
		for(var i = 0; i < sitesDataArr.length; i++)
			if(window.location.hostname.indexOf(sitesDataArr[i].hostname) >= 0)
			{
				siteData = sitesDataArr[i].items;
				if(sitesDataArr[i].hasOwnProperty("permissive"))
					bPermissive = sitesDataArr[i].permissive;

				var bUseSelectors = true; // Rare property, most won't have it; default is true 
				if(sitesDataArr[i].hasOwnProperty("useSelectors"))
					bUseSelectors = sitesDataArr[i].useSelectors;

				if(bUseSelectors)
					funcUncacatify = UncacatifyBySelector;
				else
					funcUncacatify = UncacatifyByClass;

				break;
			}

		funcUncacatify();
	});
	
	observer.observe(document, { childList:true, subtree:true });
});

var cacatometruThreshold = 0;
function CacatOMetru(headline)
{
	// TODO: something like actually smart 
	for(var i = 0; i < cacatData.sensitive.length; i++)
	{
		if(headline.indexOf(cacatData.sensitive[i]) >= 0)
		{
			console.log(headline + "*********" + cacatData.sensitive[i]);
			return 1;
		}
	}

	for(var i = 0; i < cacatData.insensitive.length; i++)
	{
		if(headline.toLowerCase().indexOf(cacatData.insensitive[i].toLowerCase()) >= 0)
		{
			console.log(headline + "*********" + cacatData.insensitive[i]);
			return 1;
		}
	}

	if(!bPermissive)
		for(var i = 0; i < cacatData.risky.length; i++)
		{
			if(headline.indexOf(cacatData.risky[i]) >= 0)
			{
				console.log(headline + "*********" + cacatData.risky[i]);
				return 1;
			}
		}

	return 0;
}